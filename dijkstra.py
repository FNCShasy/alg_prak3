import osmnx as ox
import numpy as np
import queue
import math
import priority_dict
from visualization import plot_path

map_graph = ox.graph.graph_from_address('Gummersbach, Steinmüllerallee 1, Germany', dist=5000, network_type='bike')

# Breiten- und Längengrad
origin_point = (50.985108, 7.542490)
destination_point = (51.022255, 7.562705) #Das Ziel, der Campus Gummersbach

origin = ox.distance.nearest_nodes(map_graph, origin_point[1], origin_point[0])
destination = ox.distance.nearest_nodes(map_graph, destination_point[1], destination_point[0])

# Breiten- und Längengrad
origin_point = (50.985108, 7.542490)
destination_point = (51.022255, 7.562705) #Das Ziel, der Campus Gummersbach

origin = ox.distance.nearest_nodes(map_graph, origin_point[1], origin_point[0])
destination = ox.distance.nearest_nodes(map_graph, destination_point[1], destination_point[0])

print(origin)
print(destination)

shortest_path = ox.distance.shortest_path(map_graph, origin, destination, weight='length')

plot_path(map_graph, shortest_path, origin_point, destination_point)

def dijkstras_search(origin_key, goal_key, graph):
    """
    Finden des kürzesten Weges durch Einsatz des Dijkstra Algorithmus.

    Param:
    -----

    origin_key = Start Knoten
    goal_key = Zielpunkt
    graph = OpenStreetMap Graph

    Return:
    ------
    kürzester Pfad zum Ziel

    """
    known = set()
    edgeTo = {}

    priority_queue = priority_dict.priority_dict()
    priority_queue.setdefault(origin_key, 0)

    while bool(priority_queue) is True:
        u, u_dist = priority_queue.pop_smallest()
        known.add(u)

        for edge in graph.out_edges([u], data=True):
            v = edge[1]
            if v not in known:
                old_dist = priority_queue.get(v)
                new_dist = u_dist + edge[2]['length']
                if old_dist is None or new_dist < old_dist:
                    edgeTo[v] = u
                    priority_queue[v] = new_dist

    return get_path(origin_key, goal_key, edgeTo)

# This function follows the predecessor
# backpointers and generates the equivalent path from the
# origin as a list of vertex keys.
def get_path(origin_key, goal_key, predecessors):
    key = goal_key
    path = [goal_key]

    while key != origin_key:
        key = predecessors[key]
        path.insert(0, key)

    return path

path = dijkstras_search(origin, destination, map_graph)
plot_path(map_graph, path, origin_point, destination_point)
